# README #

###Faça um git clone, entre na pasta e rode git checkout develop

### Requisitos

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Instalação

##### Configurar ambiente local
```bash
$ cp .env.example .env
```
Você pode alterar a porta da aplicação em .env
```env
DC_WEB_PORT=80
```

##### Subir ambiente de desenvolvimento
```bash
$ docker-compose up -d --build
```

##### Instalar dependencias bachend
```bash
$ docker-compose exec app composer install --prefer-dist
```

##### Configurar ambiente
```bash
$ docker-compose exec app php artisan key:generate
```

##### Criar banco de dados
```bash
$ docker-compose exec app php artisan migrate --seed
```

##### Instalar dependencias frontend
```bash
$ docker-compose exec app npm install
```

##### Rodar testes
```bash
$ docker-compose exec app vendor/bin/phpunit
```
##### Permitir escrita de logs
```bash
$ docker-compose exec app chmod -R 777 storage
```

##### Observe os serviços e acessos
```bash
$ docker-compose ps
```

##### Padrões de implementação:

- Crie branchs's sempre a partir da develop com exceção de correções em produção(Bugfix), neste caso partir da master:
    - Feature, Release, Bugfix e Hotfix / nome_significativo
- Faça commit's com base [nesta documentação](https://www.conventionalcommits.org/en/v1.0.0-beta.4/); 
- As descrições dos commit's devem ser compreendidas também por não programadores;
- Sempre atualize seu repositório local antes de push/pull-request;
- Toda codificação deve ser escrita em inglês com exceção das URI's GET e palavras sem tradução;
- Código deve seguir as especificações de estilo PSR-2;
- Código deve seguir ao máximo possível os princípios SOLID;
- Código deve conter cobertura de testes unitários e integração.

### Check-list pre-commit

##### O código está sem erro de sintaxe?
```bash
$ vendor/bin/phplint -c ./phplint.yml
```
##### O código não tem duplicação?
```bash
$ vendor/bin/phpcpd ./app
```
##### O código está limpo?
```bash
$ vendor/bin/phpmd ./app text cleancode,design,controversial,codesize,unusedcode,naming
```
##### O código segue o estilo PSR-2?
```bash
$ vendor/bin/phpcs
```
##### Os testes está passando sem erro?
```bash
$ vendor/bin/phpunit
```
##### As dependencias estão sem vulnerabilidade?
```bash
$ vendor/bin/security-checker security:check ./composer.lock
```
##### Removeu todos debugs de PHP e Javascript?